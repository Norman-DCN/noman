<!doctype html>
<html lang="en">
<head>
    <meta charset="iso-8859-1">
    <title>Noman</title>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        body{font-family:Arial;}
        img{
            margin: 0 auto;
        }
        #content{width:600px; border:1px solid grey;padding:15px; margin:0px auto;}
    </style>
</head>

<body>
<div id="content">

    <h1>Noman Jabbar</h1>
    <p>Noman Jabbar was op 4 september 2019 begonnen aan zijn <strong>HBO opleiding (Hoger Beroeps Onderwijs)</strong>. In de eerste week van zijn frisse start op het hva, kreeg hij de opdracht dat hij een vette website moest laten zien aan zijn mede studenten. Zo hij dacht: "Hoe maak ik een vette website?" wat is vet om te laten zien aan studenten die nog niet eerder hebben geprogrammeerd? Ik begon dus met een simpel design, zwart op wit, daarna stap voor stap wat elementen een kleurtje geven, want tegenwoordig gaat <abbr title="HyperText Markup Language">HTML</abbr> samen met
        <abbr title="Cascading StyleSheets">CSS</abbr> en <abbr title="JavaScript, ook wel gebaseerd op ECMAScript">JS</abbr>.<br>


    <h2>Goed en slecht</h2>

    <p>met mijn ervaring, heb ik <a target="_blank" href="http://www.angelfire.com/falcon/flamingchickens/abcd.html">slechte</a> en
        <a href="https://nl.reviewclub.com/nl">goede</a> websites gezien. wat is een goede website voor mij? het moet <b>simpel</b> zijn, maar modern en de gebruiker moet er niet in de war raken door de <mark><strong>layout van de website</strong></mark>.</p>

    <h2>Skills</h2>

    de volgende skills heb ik geleerd met hulp van leraren op het mbo in de afgelopen 3 jaar (naast de skills die ik thuis heb aangeleerd op het internet):
    <ul>
        <li>HTML</li>
        <li>CSS</li>
        <li>Vanilla JS</li>
        <li>Blade</li>
        <li>Twig</li>
        <li>Sass</li>
        <li>Less</li>
        <li>Bootstrap</li>
        <li>Drupal</li>
        <li>Wordpress</li>
        <li>JQuery</li>
        <li>PHP</li>
        <li>Python</li>
        <li>Java</li>
        <li>CLI</li>
        <li>Android</li>
        <li>Laravel</li>
        <li>een klein beetje <abbr title="wees bij irfan voor een vette ruby website">ruby</abbr></li>
        <li>Symphony</li>
        <li>API Integration</li>
        <li>SEO en SEA</li>
    </ul>

    <a href="../../"><img src="{{ $url }}" alt=""></a>
</div>

</body>
</html>
