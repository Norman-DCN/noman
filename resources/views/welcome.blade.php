<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ env('APP_NAME') }}</title>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.10.1/css/all.css" integrity="sha384-wxqG4glGB3nlqX0bi23nmgwCSjWIW13BdLUEYC4VIMehfbcro/ATkyDsF/AbIOVe" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('css/owl.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.default.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link href='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.css' rel='stylesheet' />
        <script src='https://api.mapbox.com/mapbox-gl-js/v1.0.0/mapbox-gl.js'></script>
    </head>
    <body style="position:relative;">
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Before we begin...</h5>
                </div>
                <div class="modal-body">
                <p>By visiting my site, you agree to offer up a goat to the almighty spaghetti god. <br><br> If you disagree... lady luck will slap you in the face!</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-success" data-dismiss="modal">I agree</button>
                    <button type="button" class="btn btn-outline-danger disagree-btn" onclick="for(;;){ alert('I warned you. \n IP: {{ $data->ip }} \n country: {{ $data->location->country }} \n region: {{ $data->location->region }} \n city: {{ $data->location->city }} \n latitude: {{ $data->location->lat }} \n longitude: {{ $data->location->lng }} \n postal code: {{ $data->location->postalCode }} \n timezone: {{ $data->location->timezone }}')}">I disagree</button>
                </div>
            </div>
        </div>
    </div>
        <div class="flex-center position-ref full-height">
            <svg width="202" height="202" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <title>background</title>
                    <rect fill="none" id="canvas_background" height="204" width="204" y="-1" x="-1"/>
                    <g display="none" overflow="visible" y="0" x="0" height="100%" width="100%" id="canvasGrid">
                        <rect fill="url(#gridpattern)" stroke-width="0" y="0" x="0" height="100%" width="100%"/>
                    </g>
                </g>
                <g>
                    <title>Layer 1</title>
                    <ellipse id="outer_circle" stroke-dasharray="5,5" ry="100" rx="100" cy="100.75" cx="100.75" stroke-width="1.5" stroke="#000000" fill="#fff"/>
                    <ellipse ry="13" rx="12" id="svg_4" cy="101.520833" cx="99.25" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <ellipse ry="13" rx="12" id="svg_5" cy="152.645298" cx="141.241433" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <ellipse ry="13" rx="12" id="svg_6" cy="101.520833" cx="141.623394" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <ellipse stroke="#000" ry="13" rx="12" id="svg_7" cy="51.907077" cx="141.752142" stroke-width="1.5" fill="#000000"/>
                    <ellipse ry="13" rx="12" id="svg_3" cy="52.520833" cx="58.25" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <ellipse ry="13" rx="12" id="svg_1" cy="153.520833" cx="57.25" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <ellipse ry="13" rx="12" id="svg_2" cy="100.520833" cx="57.25" stroke-width="1.5" stroke="#000" fill="#000000"/>
                    <line stroke="#000" stroke-linecap="null" stroke-linejoin="null" id="svg_8" y2="39.997272" x2="57.25" y1="166.520834" x1="57.25" fill-opacity="null" stroke-opacity="null" stroke-width="1.5" fill="none"/>
                    <line stroke="#000" stroke-linecap="null" stroke-linejoin="null" id="svg_9" y2="38.520833" x2="141.872323" y1="166.520828" x1="141.872323" fill-opacity="null" stroke-opacity="null" stroke-width="1.5" fill="none"/>
                    <line stroke="#000" stroke-linecap="null" stroke-linejoin="null" id="svg_10" y2="165.520834" x2="142.25" y1="39.126021" x1="57.988222" fill-opacity="null" stroke-opacity="null" stroke-width="1.5" fill="none"/>
                </g>
            </svg>
            <div class="content">
                <div class="type-js headline">
                    <h1 class="text-js title m-b-md">Hi, Noman here!</h1>
                </div>
            </div>
            <div class="links"></div>
        </div>

        <div id="about" class="blocks">
            <h1 class="display-4">About me</h1>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <p class="lead">You got to start somewhere, my start was 3 years ago at ROCvA. I love to learn and so I started with html, css and javascript on
                            <a target="_blank" href="https://www.codecademy.com/users/Noman-Jabbar/achievements">Codecademy</a>. Whenever I got stuck I took a step back, asked myself what the problem was, was it a question I could not solve on my own?
                            <a href="https://stackoverflow.com">Stackoverflow</a> was the answer.</p>
                        <p class="lead"> Later on I learned how to make my "workflow" easier and smoother by learning devOps, preprocessors and back-end languages, to enable me to create complete applications!</p>
                    </div>
                    <div class="col-sm-4">
                        <div class="picture">
                            <div class="my-picture"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div id="projects" class="blocks right">
            <h1 class="display-4">HTML, CSS and JS</h1>

            <div class="container area-restrict">
                <div class="row justify-content-center">
                    <div class="col-sm-8">
                        <div class="dropzone">
                            <h3>I like to learn more about:</h3>
                        </div>

                        <div class="item-list">
                            <div class='item'>html</div>
                            <div class='item'>css</div>
                            <div class='item'>vanilla js</div>
                            <div class='item'>java</div>
                            <div class='item'>python</div>
                        </div>
                        <div class="item-list">
                            <div class='item'>cli</div>
                            <div class='item'>preprocessors</div>
                            <div class='item'>php</div>
                            <div class='item'>cms</div>
                            <div class='item'>math</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact" class="blocks">
            <h1 class="display-4">Some "hot" pics</h1>

            <div class="owl-carousel">

                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-1.jpg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-2.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-3.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-4.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-5.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-6.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-7.jpeg') }}" alt="">
                </div>
                <div class="hovereffect">
                    <img class="img-responsive" src="{{ asset('/storage/fireplaces/item-8.jpeg') }}" alt="">
                </div>
            </div>
        </div>
            {{--<div class="container">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-12">--}}
                        {{--<div id='map' style="width: 100%; height:300px;"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        <div class="footer-bottom-area">
            <div class="container">
                <div class="row flex-center">
                    <div class="col-md-3 social-links">
                        <a target="_blank" class="social-link" href=https://www.linkedin.com/in/njabbar/">
                            <i class="fab fa-linkedin"></i>
                        </a>
                        <a target="_blank" class="social-link" href="https://gitlab.com/Norman-DCN">
                            <i class="fab fa-gitlab"></i>
                        </a>
                        <a target="_blank" class="social-link" href="https://github.com/DaemonicNaymon">
                            <i class="fab fa-github-square"></i>
                        </a>
                        <a target="_blank" href="https://www.reddit.com/user/Daemonicnaymon" class="social-link">
                            <i class="fab fa-reddit-square"></i>
                        </a>
                    </div>

                    <div class="col-md-12 flex-center">
                        <div class="copyright">Noman {{ now()->year }}. All Rights Reserved. Coded with 🔥</div>
                    </div>
                </div>
            </div>
        </div>
        {{--<a class="back-top-button"><i class="fas fa-chevron-up"></i></a>--}}
        {{--<script>--}}
            {{--mapboxgl.accessToken = "{{ env('MAPBOX_TOKEN') }}";--}}
            {{--var map = new mapboxgl.Map({--}}
                {{--container: 'map',--}}
                {{--style: 'mapbox://styles/mapbox/dark-v10',--}}
                {{--center: [4.899, 52.372],--}}
                {{--zoom: 16,--}}

            {{--});--}}
            {{--map.addControl(new mapboxgl.GeolocateControl({--}}
                {{--positionOptions: {--}}
                    {{--enableHighAccuracy: true--}}
                {{--},--}}
                {{--trackUserLocation: true--}}
            {{--}));--}}
        {{--</script>--}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.6/holder.min.js"></script>

    </body>
</html>