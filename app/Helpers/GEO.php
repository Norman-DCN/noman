<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 9/3/2019
 * Time: 9:10 PM
 */

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;

class GEO
{

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://geo.ipify.org',
        ]);

    }


    public function showDetails($ip)
    {
        $this->response = $this->client->get("/api/v1?apiKey=at_8vSGAD0YkZGqm3asiLHJ4rJ7ZcLuT&ipAddress=$ip", []);

        $this->data = json_decode($this->response->getBody());

        return $this;
    }

}