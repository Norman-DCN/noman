<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 9/3/2019
 * Time: 9:10 PM
 */

namespace App\Helpers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Redirect;

class QR
{

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.ipify.org',
        ]);

    }


    public function createQR($url)
    {
        $url = urlencode($url);

        return "https://api.qrserver.com/v1/create-qr-code/?size=550x550&data=$url";

    }

    public function showIp()
    {
        $this->response = $this->client->get('?format=json', []);

        $this->data = json_decode($this->response->getBody());

        return $this;
    }

}