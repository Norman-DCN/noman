<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class QRServiceProvider extends ServiceProvider
{

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('qr', function() {

            return new \App\Helpers\QR;

        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
