<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;


class GEO extends Facade {



    protected static function getFacadeAccessor()
    {
        return 'geo';
    }
}

