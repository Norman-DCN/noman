<?php

namespace App\Http\Controllers;

use App\Facades\QR;
use Illuminate\Http\Request;

class QRController extends Controller
{
    public function create()
    {
        return view('qr.create');
    }

    public function submit()
    {
        $url = QR::createQR(request()->get('url'));
        return view('qr.show', ['url' => $url]);
    }
}
