<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Facades\GEO;
use App\Facades\QR;

Route::get('/', function () {

    $ip = QR::showIp()->data->ip;

    $data = GEO::showDetails($ip)->data;

    return view('welcome', ['data' => $data]);
});


Route::get('qr', 'QRController@create');

Route::post('qr/create', 'QRController@submit')->name('submit.qr');